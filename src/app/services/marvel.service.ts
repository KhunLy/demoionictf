import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Md5 } from 'ts-md5/dist/md5';
import { MarvelRequest } from '../models/marvel-request.model';

@Injectable({
  providedIn: 'root'
})
export class MarvelService {

  constructor(
    private httpClient: HttpClient
  ) { }

  get(offset:number = 0) : Observable<MarvelRequest> {
    let params = new HttpParams();
    params = params.append('offset', offset);
    
    /** authorization */
    let ts = Date.now(); // get the current timestamp
    params = params.append('ts', ts);
    params = params.append('apikey', environment.pubk);
    params = params.append('hash', Md5.hashStr(ts + environment.prik + environment.pubk));
    // end authorization
    
    return this.httpClient.get<MarvelRequest>(environment.marvelAPI, { params });
  }

  getDetails(id: number) : Observable<MarvelRequest>{
    let params = new HttpParams();
    
    /** authorization */
    let ts = Date.now(); // get the current timestamp
    params = params.append('ts', ts);
    params = params.append('apikey', environment.pubk);
    params = params.append('hash', Md5.hashStr(ts + environment.prik + environment.pubk));
    // end authorization
    
    return this.httpClient.get<MarvelRequest>(environment.marvelAPI + '/' + id, { params });
  }
}
