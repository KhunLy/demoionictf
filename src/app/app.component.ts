import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  menu: any[];

  constructor(
    private menuCtrl: MenuController,
    private storage: Storage,
  ) {}

  ngOnInit(): void {
    this.storage.create();
    this.menu = [
      { path: '/home', title: 'Accueil', icon: 'home' },
      { path: '/about', title: 'A Propos', icon: 'star' },
      { path: '/cart', title: 'Panier', icon: 'cart' },
      { path: '/marvel', title: 'Marvel', icon: 'book' },
    ]
  }

  collapse() {
    this.menuCtrl.close();
  }
}
