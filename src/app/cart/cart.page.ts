import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Article } from '../models/article.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  articles: Article[];
  newArticle: string;

  constructor(
    private actionSheetCtrl : ActionSheetController,
    private toastService: ToastController,
    private alertService: AlertController,
    private storage: Storage,
  ) { }

  ngOnInit() {
    //this.articles = [];
    this.load();
  }

  add() {
    if(!this.newArticle?.trim()) return;
    this.articles = [...this.articles, 
      { name: this.newArticle, isChecked: false }
    ];

    this.save();

    this.toastService.create({
      message: `L'article ${this.newArticle} a été ajouté au panier`,
      duration: 500,
      color: 'success',
      position: 'top',
    }).then(t => t.present());

    this.newArticle = null;
  }

  displayActions(item: Article) {
    this.actionSheetCtrl.create({
      header: 'Actions',
      buttons: [
        {
          text: item.isChecked ? 'Décocher' : 'Cocher',
          icon: item.isChecked ? 'remove' : 'checkmark',
          handler: () => {
            item.isChecked = !item.isChecked;
            this.save();
          }
        },{
          text: 'Supprimer',
          icon: 'trash',
          role: 'destructive',
          handler: () => {
            // let i = this.articles.indexOf(item);
            // this.articles.splice(i, 1);
            this.articles = this.articles.filter(a => a !== item);
            this.save();
          }
        },{
          text: 'Annuler',
          icon: 'close',
          role: 'cancel'
        }
      ]
    }).then(as => as.present());
  }

  removeAll() {
    this.alertService.create({
      header: 'Vous êtes sur le point de tout supprimer, voulez continuer ?',
      buttons: [
        'Non',
        { text: 'Oui', handler: () => {
          this.articles = [];
          this.clear();
        } }
      ]
    }).then(a => a.present());
  }

  private save() {
    this.storage.set('ARTICLES', this.articles);
  }

  private clear() {
    this.storage.remove('ARTICLES');
  }

  private async load() {
    this.articles = await this.storage.get('ARTICLES') || [];
  }

  // async displayActions() {
  //   const as = await this.actionSheetCtrl.create({
  //     header: 'Actions',
  //     buttons: [
  //       {
  //         text: 'Valider'
  //       },{
  //         text: 'Supprimer',
  //         role: 'destructive'
  //       },{
  //         text: 'Annuler',
  //         role: 'cancel'
  //       }
  //     ]
  //   })
  //   as.present();
  // }

}
