import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Result } from '../models/marvel-request.model';
import { MarvelService } from '../services/marvel.service';
import { MarvelDetailsComponent } from './marvel-details/marvel-details.component';

@Component({
  selector: 'app-marvel',
  templateUrl: './marvel.page.html',
  styleUrls: ['./marvel.page.scss'],
})
export class MarvelPage implements OnInit {

  heroes: Result[];
  currentOffset:number = 0;

  constructor(
    private marvelService: MarvelService,
    private modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    this.marvelService.get().subscribe(request => {
      this.heroes = request.data.results;
    })
  }

  loadData(event) {
    console.log(42);
    
    this.marvelService.get(this.currentOffset + 20).subscribe(request => {
      this.currentOffset += 20;
      // this.heroes.push(...request.data.results);
      this.heroes = [...this.heroes, ...request.data.results];
      event.target.complete();
    })
  }

  showDetails(id: number) {
    this.modalCtrl.create({ 
      component: MarvelDetailsComponent,
      componentProps: { id }
    }).then(m => m.present());
  }

}
