import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Result } from 'src/app/models/marvel-request.model';
import { MarvelService } from 'src/app/services/marvel.service';

@Component({
  selector: 'app-marvel-details',
  templateUrl: './marvel-details.component.html',
  styleUrls: ['./marvel-details.component.scss'],
})
export class MarvelDetailsComponent implements OnInit {

  @Input()
  id: number;

  hero: Result;

  constructor(
    private modalCtrl: ModalController,
    private marvelService : MarvelService
  ) { }


  ngOnInit() {
    this.marvelService.getDetails(this.id).subscribe(request => {
      if(!request.data.results.length) 
        return;
      this.hero = request.data.results[0];
    });
  }

  back() {
    this.modalCtrl.dismiss();
  }
}
